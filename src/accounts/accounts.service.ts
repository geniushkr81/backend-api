import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Account } from './interfaces/account.interface';
import { AccountDTO } from './dto/account.dto';

@Injectable()
export class AccountsService {
    // constructor
    constructor(@InjectModel('Account') private readonly AccountModel: Model<Account>){}
    // get all accounts
    async getAll(): Promise<any>{
        return await this.AccountModel.find();
    }
    // create new account
    async create(AccountDTO: AccountDTO): Promise<any> {
        const createAccount = new this.AccountModel(AccountDTO);
        return createAccount.save();
    }
}
