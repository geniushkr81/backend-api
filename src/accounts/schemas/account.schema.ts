import * as mongoose from 'mongoose';

export const AccountSchema = new mongoose.Schema({
    code:String,
    name:String,
    curp:String,
    nss:String,
    email:String,
},{timestamps: true});