import { Controller,Post,Body,Query, Param,Put, NotFoundException, 
    Get, Res, HttpStatus, UseGuards} from '@nestjs/common';
import { AccountsService } from './accounts.service';
import { AccountSchema } from './schemas/account.schema';
import { AccountDTO } from './dto/account.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { AuthGuard } from '@nestjs/passport';

@Controller('accounts')
export class AccountsController {
    constructor(private account:AccountsService){}
    // create new record account
    @Post('/create')
    async addAccount(@Res() res, @Body() AccountDTO:AccountDTO){
        const accounts = await this.account.create(AccountDTO);
        return res.status(HttpStatus.OK).json({
            message:" Account has been created Successfully",
            accounts
        });
    }
    // get all records accounts
    @Get('all')
    async GetAll(@Res() res){
        const accounts = await this.account.getAll();
        console.log(accounts);
        return res.status(HttpStatus.OK).json(accounts);
    }
}
