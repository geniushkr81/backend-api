export class AccountDTO {
    readonly code:string;
    readonly name:string;
    readonly curp:string;
    readonly nss:string;
    readonly email:string;
}