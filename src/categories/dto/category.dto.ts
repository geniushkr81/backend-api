// import { ApiProperty } from '@nestjs/swagger';
export class CategoryDTO {
    readonly code:string;
    readonly name:string;
    readonly details:string;
    readonly image:string;
}