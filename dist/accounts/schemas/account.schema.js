"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountSchema = void 0;
const mongoose = require("mongoose");
exports.AccountSchema = new mongoose.Schema({
    code: String,
    name: String,
    curp: String,
    nss: String,
    email: String,
}, { timestamps: true });
//# sourceMappingURL=account.schema.js.map