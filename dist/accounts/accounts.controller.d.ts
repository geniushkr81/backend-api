import { AccountsService } from './accounts.service';
import { AccountDTO } from './dto/account.dto';
export declare class AccountsController {
    private account;
    constructor(account: AccountsService);
    addAccount(res: any, AccountDTO: AccountDTO): Promise<any>;
    GetAll(res: any): Promise<any>;
}
