import { Model } from 'mongoose';
import { Account } from './interfaces/account.interface';
import { AccountDTO } from './dto/account.dto';
export declare class AccountsService {
    private readonly AccountModel;
    constructor(AccountModel: Model<Account>);
    getAll(): Promise<any>;
    create(AccountDTO: AccountDTO): Promise<any>;
}
